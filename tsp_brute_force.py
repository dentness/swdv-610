# SWDV-610-2018-2W
# Joe Dent
# Week 8 - Traveling Salesperson 
import itertools

def __calcPathLength__( g, path ):
    """  
        Desription:
        Given a path to follow, it uses the weight values for each edge
        and totals the length of the path.

        Params:
        g - Graph object holding the verticies
        path - An iterable holding the vertex ids in visitation order

        Return: 
        totalLength - The calculated total of the edge distances along the path
    """    
    if len(path) <= 1:
        # No verticies, no length
        return 0

    if len(path) == 2:
        # Only one edge
        v = g.getVertex(path[0])
        return v.getWeight(path[1]) * 2

    totalLength = 0
    for x in range( len(path) - 1 ):
           v1 = g.getVertex(path[x])
           v2 = g.getVertex(path[x+1])
           totalLength += v1.getWeight(v2)
    
    return totalLength

def bruteForceSolution( g, homeVertex ):
    """
        Description:
        Build a graph with all of the vertices and weights,
        then run exhaustive searches to capture the total
        weight of each path.  This is not efficient but
        computers are good for this type of thing, as long
        as the number of nodes is low (<10).

        Parameters:
        g - a hydrated graph object to be traversed.
        homeVertex - ID of the vertex to select as the home for the salesperson.

        Returns:
        shortestPath    The shortest path found for the graph, in the form of a node list:
                        [1,2,3,4,5,1] which translates to the path 1-2-3-4-5-1.
        totalLength     The length associated with the shortestPath.

        
    """
    nodes = g.getVertices()

    #filter so only the tours that start and end with the homeVertex are used.
    paths = list(filter(lambda x: list(x)[0] == homeVertex, itertools.permutations(nodes)))

    shortestPath = []
    totalLength = 999999999 # Large number... can't find inf anymore.

    for path in paths:
        path = list(path)
        path.append(path[0])
        w = __calcPathLength__(g, list(path))
        if w < totalLength:
            shortestPath = path
            totalLength = w

    return shortestPath, totalLength