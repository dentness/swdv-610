# SWDV-610-2018-2W
# Joe Dent
# Week 8 - Traveling Salesperson 

# CODE PROVIDED BY TEXTBOOK: Problem Solving with Algorithms and Data Structures: Ch 7.6
#   Adjusted some parameter names for my own sensibilities
#

class Vertex:
    def __init__(self,key):
        self.id = key
        self.connectedTo = {}

    def addNeighbor(self, nbr, weight=0):
        self.connectedTo[nbr] = weight

    def __str__(self):
        return str(self.id) + ' connectedTo: ' + str([x.id for x in self.connectedTo])

    def getConnections(self):
        return self.connectedTo.keys()

    def getId(self):
        return self.id

    def getWeight(self, nbr):
        return self.connectedTo[nbr]

class Graph:
    def __init__(self):
        self.vertList = {}
        self.numVertices = 0

    def addVertex(self, key):
        self.numVertices = self.numVertices + 1
        newVertex = Vertex(key)
        self.vertList[key] = newVertex
        return newVertex

    def getVertex(self, key):
        if key in self.vertList:
            return self.vertList[key]
        else:
            return None

    def __contains__(self, key):
        return key in self.vertList

    def addEdge(self, fKey, tKey, weight=0):
        if fKey not in self.vertList:
            self.addVertex(fKey)
        if tKey not in self.vertList:
            self.addVertex(tKey)
        self.vertList[fKey].addNeighbor(self.vertList[tKey], weight)

    def getVertices(self):
        return self.vertList.keys()

    def __iter__(self):
        return iter(self.vertList.values())
