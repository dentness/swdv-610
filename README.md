###Traveling Salesperson Problem


This is a classic "toy" problem in Computer Science where a sales person has the need to travel to a number of cities, and wants to find the shortest route to do so, without revisiting any city.

The challenge with this problem is that there is no polynomial solution to it; that is, no algorithm has been implemented yet that is proven to solve this in the optimal steps.  Solutions can 
be found, and this project will show the brute force method for finding a solution by creating all of the permutations of nvertexodes starting with home vertex and returning to the home vertex. 
The home node is arbitrary, but for the sake of this application will always be the first vertex

The type of problem that is the Traveling Salesperson is a minimum-weight Hamiltonian cicuit.(1)  This means that the graph is created so that ever pair of vertices has an edge between them, that we use every vertex in the circuit, and that we account for the weight or cost for traveling along the edges during our tour.



(1) [http://jlmartin.faculty.ku.edu/~jlmartin/courses/math105-F11/Lectures/chapter6-part3.pdf](http://jlmartin.faculty.ku.edu/~jlmartin/courses/math105-F11/Lectures/chapter6-part3.pdf)