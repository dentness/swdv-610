# SWDV-610-2018-2W
# Joe Dent
# Week 8 - Traveling Salesperson 

from graphs import Vertex, Graph
from tsp_brute_force import bruteForceSolution
import random
import itertools


def buildVertex(g, v1ID, v2ID, distance):
    """  
        Desription:
        Creates an edge in both directions for the same set of verticies
        with the same weight in both directions. This will allow the 
        traversal to start with either side of the edge.

        Params:
        g - Graph object holding the verticies
        v1ID - Text ID of the starting vertex
        v2ID - Text ID of the destination vertex
        weight - The 'distance' between the verticies

        Return: 
        None
    """
    g.addEdge(v1ID, v2ID, distance)
    g.addEdge(v2ID, v1ID, distance)

def buildGraph( numOfVerticies, min, max ):
    """
        Description:
        Function to create a Graph object and 
        populate it by hand.

        Params:
        lst - List of vertex ids
        min - Minimum random value for the weight
        max - Maximum random value for the weight

        Return:
        Graph object, fully populated.
    """
    g = Graph()

    lst = []
    for x in range(numOfVerticies):
        lst.append(str(x))

    c = itertools.combinations(lst, 2)
    [buildVertex(g, x, y, random.randint(min, max)) for x,y in c]

    return g


def reportSolutionPath( title, homeVertex, numberOfVertices, path, length ):
    """
        Description:
        Simple report printing for a path

        Params:
        title - Header for the output
        homeVertex - The vertex that the salesperson starts from and returns to.
        numberOfVertices - The total number of cities (vertices) the salesperson visits
        path - The TSP path to be presented
        length - The total length of the path

        Results:
        None
    """
    print("{}\n\tNumber of Cities: {}\n\tHome: {}\n\tShortest Path: {}\n\tLength: {}\n".format(title, numberOfVertices, homeVertex, path, length))

def main():
    numberOfVertices = 11
    minimumDistance = 50
    maximumDistance = 500
    homeVertex = str(random.randint(1,numberOfVertices))
    g = buildGraph(numberOfVertices, minimumDistance, maximumDistance)

    shortestPathBF, lengthBF = bruteForceSolution( g, homeVertex ) 
    reportSolutionPath("Traveling Salesperson Problem By Brute Force", homeVertex, numberOfVertices, shortestPathBF, lengthBF)


main()